import itertools


def imap(fun, *args):
    if not callable(fun):
        raise TypeError('The first argument must be a function')

    _args_iterable = []
    for arg in args:
        _args_iterable.append(iter(arg))

    if len(args) > 1:
        min_len = min(*[len(arg) for arg in args])
        for _ in range(min_len):
            result = [next(it) for it in _args_iterable]
            yield fun(*result)
    else:
        for item in args[0]:
            yield fun(item)


assert (list(imap(lambda x: x + x, (1, 2, 3, 4))) == [2, 4, 6, 8])
assert (list(imap(lambda x, y: x + y, [1, 2, 3], [4, 5, 6, 7, 8])) == [5, 7, 9])
assert (list(imap(list, ['sat', 'bat', 'cat', 'mat'])) ==
        [['s', 'a', 't'], ['b', 'a', 't'], ['c', 'a', 't'], ['m', 'a', 't']])
assert (list(imap(str.upper, ['one', 'two', 'list', '', 'dict'])) == ['ONE', 'TWO', 'LIST', '', 'DICT'])
assert (list(imap(pow, [1, 2, 3], [4, 5, 6, 7])) == [1, 32, 729])

print('SUCCESS!')
