import itertools


def frange(*args):
    if len(args) == 1:
        start, end, step = 0, args[0], 1
    elif len(args) == 2:
        start, end, step = *args, 1
    elif len(args) == 3:
        start, end, step = args[0], args[1], args[2]

    temp_result = start
    pointer = 0

    def _check_data(step_sign):
        if step_sign:
            if temp_result + step < end:
                return True
        else:
            if temp_result + step > end:
                return True

    while _check_data(step > 0):
        temp_result = start + step * pointer
        pointer += 1
        yield temp_result


print(list(frange(5)))

assert(list(frange(5)) == [0, 1, 2, 3, 4])
assert(list(frange(2, 5)) == [2, 3, 4])
assert(list(frange(2, 10, 2)) == [2, 4, 6, 8])
assert(list(frange(10, 2, -2)) == [10, 8, 6, 4])
assert(list(frange(2, 5.5, 1.5)) == [2, 3.5, 5])
assert(list(frange(1, 5)) == [1, 2, 3, 4])
assert(list(frange(0, 5)) == [0, 1, 2, 3, 4])
assert(list(frange(0, 0)) == [])
assert(list(frange(100, 0)) == [])
assert(list(itertools.islice(frange(0, float(10**10), 1.0), 0, 4)) == [0, 1.0, 2.0, 3.0])

print('SUCCESS!')