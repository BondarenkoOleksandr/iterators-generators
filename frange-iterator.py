import itertools


class frange:
    def __init__(self, *args):
        if len(args) == 1:
            start, end, step = 0, args[0], 1
        elif len(args) == 2:
            start, end, step = *args, 1
        elif len(args) == 3:
            start, end, step = args[0], args[1], args[2]

        if step == 0:
            raise ValueError('Step must be different from 0')

        self.start = start
        self.end = end
        self.step = step

    class FrangeIterator:
        def __init__(self, iterable):
            self.iterable = iterable
            self._pointer = 0
            self._temp_result = self.iterable.start

        def _calculate_data(self):
            self._temp_result = self.iterable.start + self.iterable.step * self._pointer
            self._pointer += 1

        def _check_data(self, step_sign):
            if step_sign:
                if self._temp_result + self.iterable.step >= self.iterable.end:
                    raise StopIteration()
            else:
                if self._temp_result + self.iterable.step <= self.iterable.end:
                    raise StopIteration()

        def _check_step(self):
            self._check_data(self.iterable.step > 0)

        def __next__(self):
            self._check_step()
            self._calculate_data()
            return self._temp_result

    def __iter__(self):
        return self.FrangeIterator(self)


assert(list(frange(5)) == [0, 1, 2, 3, 4])
assert(list(frange(2, 5)) == [2, 3, 4])
assert(list(frange(2, 10, 2)) == [2, 4, 6, 8])
assert(list(frange(10, 2, -2)) == [10, 8, 6, 4])
assert(list(frange(2, 5.5, 1.5)) == [2, 3.5, 5])
assert(list(frange(1, 5)) == [1, 2, 3, 4])
assert(list(frange(0, 5)) == [0, 1, 2, 3, 4])
assert(list(frange(0, 0)) == [])
assert(list(frange(100, 0)) == [])
assert(list(itertools.islice(frange(0, float(10**10), 1.0), 0, 4)) == [0, 1.0, 2.0, 3.0])

print('SUCCESS!')
