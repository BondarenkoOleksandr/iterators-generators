import itertools


class imap():

    def __init__(self, fun, *args):

        if not callable(fun):
            raise TypeError('The first argument must be a function')
        self.fun = fun
        self._pointer = 0
        self._args_iterable = []
        for arg in args:
            self._args_iterable.append(iter(arg))

    def __next__(self):
        result = [next(it) for it in self._args_iterable]
        self._pointer += 1
        return self.fun(*result)

    def __iter__(self):
        return self


assert (list(imap(lambda x: x + x, (1, 2, 3, 4))) == [2, 4, 6, 8])
assert (list(imap(lambda x, y: x + y, [1, 2, 3], [4, 5, 6])) == [5, 7, 9])
assert (list(imap(list, ['sat', 'bat', 'cat', 'mat'])) ==
        [['s', 'a', 't'], ['b', 'a', 't'], ['c', 'a', 't'], ['m', 'a', 't']])
assert (list(imap(str.upper, ['one', 'two', 'list', '', 'dict'])) == ['ONE', 'TWO', 'LIST', '', 'DICT'])
assert (list(imap(pow, [1, 2, 3], [4, 5, 6, 7])) == [1, 32, 729])


print('SUCCESS!')
